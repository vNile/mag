# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  $('#code').change ->
    $.ajax
      type: 'POST'
      url: '/orders/check_code'
      data: code: $('#code').val(), sum: $('#order_sum').val()
  $('#order_sum').change ->
    #учесть скидку если есть ипоказать рез
    $('#result').html('Итого: ' + $('#order_sum').val())
