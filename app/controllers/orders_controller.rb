class OrdersController < ApplicationController
  before_action :find_order, only: [:edit, :update]
  before_action :find_coupon, only: [:coupon_check, :create, :check_code]


  def index
    if current_user.has_role? :admin
      redirect_to admin_transactions_path  and return
    else
      redirect_to new_order_path and return
    end
  end

  def new
    @order = Order.new
  end

  def create
    @order = Order.new(permitted_params)
    @order.user = current_user
    if @coupon.present? && @coupon.is_active?
      @order.coupon = @coupon
      @order.sum = @coupon.calculate_sum(@order.sum)
    end
    if @order.save
      flash[:success] = 'Все хорошо'
      redirect_to new_order_path
    else
      render :new
    end
  end

  def check_code
    if @coupon.present? && @coupon.is_active?
      @result = @coupon.calculate_sum(params[:sum])
      @amount = @coupon.calculate_amount(params[:sum])
      @result = 0 if @result < 0
      flash[:notice] = 'Промо-код принят'
    else
      flash[:error] = 'Не верный промо код'
    end
  end

  private

  def find_order
    @order = Order.find(params[:id])
  end

  def find_coupon
    @coupon = Coupon.by_code(params[:code]).first
  end

  def permitted_params
    params[:order].permit(:description, :sum, :code)
  end
end
