class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  before_action :authenticate_user!
  
  protect_from_forgery with: :exception

  private

  def authorize_as_admin!
    unless current_user.has_role? :admin
      flash[:notice] = 'Вы не авторизованы для исполнения данного действия'
      redirect_to root_path and return
    end
  end
end
