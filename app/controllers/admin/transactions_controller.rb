class Admin::TransactionsController < ApplicationController
  layout 'admin'
  before_action :authorize_as_admin!

  def index
    @orders = Order.includes(:user).includes(:coupon).order(id: :desc).page(params[:page])
  end
end
