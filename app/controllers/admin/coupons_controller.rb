class Admin::CouponsController < ApplicationController
  layout 'admin'
  before_action :authorize_as_admin!

  def index
    @coupons = Coupon.order(id: :desc).page(params[:page])
  end

  def new
    @coupon = Coupon.new
  end

  def create
    count = coupon_params[:count]
    mask = coupon_params[:mask]
    charset = ('a'..'z').to_a + ('A'..'Z').to_a
    strint = (charset + (0..9).to_a).flatten
    @coupon = Coupon.new(coupon_params)
    if @coupon.valid?
      i = 1
      while i <= count.to_i do
        code = mask.gsub(/#/, rand(10).to_s).gsub(/@/, charset.sample(1).first).gsub(/\*/, strint.sample(1).first.to_s)
        @coupon = Coupon.new(coupon_params)
        @coupon.code = code
        i += 1 if @coupon.save
      end
    else
      render :new and return
    end
    redirect_to admin_coupons_path
  end

  def edit
  end

  def update
    @coupon = Coupon.find(params[:id])

    if @coupon.update(coupon_params)
      redirect_to admin_coupons_path
    else
      render :edit
    end
  end

  def destroy
    @coupon.destroy!

    redirect_to admin_coupons_path
  end

  private

  def find_coupon
    @coupon = Coupon.find(params[:id])
  end

  def coupon_params
    params
      .require(:coupon)
      .permit(:redemption_limit, :sum, :coupon_type, :mask, :count)
  end
end
