class User < ActiveRecord::Base
  rolify strict: true
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :orders
  has_many :redemptions, class_name: 'CouponRedemption'

end
