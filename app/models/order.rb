class Order < ActiveRecord::Base
  belongs_to :coupon
  belongs_to :user

  validates :description, :sum, presence: true
end
