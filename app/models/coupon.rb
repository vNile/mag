class Coupon < ActiveRecord::Base
  has_many :orders

  attr_accessor :count, :mask

  validates :mask, :sum, :count, presence: true
  validates :coupon_type, inclusion: { in: %w(percentage amount) }
  validates :code, uniqueness: true

  validates :redemption_limit, numericality: {
    greater_than_or_equal_to: 0 }

  scope :by_code, -> (code){ where(code: code) }

  def percentage_based?
    coupon_type == 'percentage'
  end

  def amount_based?
    coupon_type == 'amount'
  end

  def is_active?
    orders.count < redemption_limit
  end

  def calculate_sum(order_sum)
    order_sum.to_f - calculate_amount(order_sum).to_f
  end

  def calculate_amount(order_sum)
    if amount_based?
      sum
    else
      order_sum.to_f * sum.to_f / 100
    end
  end
end
