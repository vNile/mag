class CreateCoupons < ActiveRecord::Migration
  def change
    create_table :coupons do |t|
      t.string  :code, null: false, unique: true
      t.integer :redemption_limit, default: 1, null: false
      t.integer :sum, null: false, default: 0
      t.string  :coupon_type, null: false
      t.timestamps null: false
    end
  end
end
