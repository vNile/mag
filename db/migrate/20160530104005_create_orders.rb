class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.text :description
      t.decimal :sum, default: 0
      t.references :user
      t.references :coupon
      t.timestamps null: false
    end
  end
end
